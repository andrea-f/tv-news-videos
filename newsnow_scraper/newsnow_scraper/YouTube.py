#!/usr/bin/python

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser

import sys, os
from pprint import pprint
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "/Users/afassina/Workspace/private/NewsNow/newsnow-scraper-scrapy/newsnow_scraper/newsnow_scraper/credentials.json"
api_key = 'youtube_api_key'
with open(api_key, 'r') as outfile:
    DEVELOPER_KEY = outfile.read()
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# YOUTUBE API CLIENT https://developers.google.com/youtube/v3/docs/videos/list

class SearchVideos():
    """This class searches videos based on an event."""


    def __init__(self, event = None):
        """Does nothing for now.
        
        :param event:
            From :class:`Event`, object.            
        """
        self.event = event

    def _setUpYTApi(self):
        """Sets up YouTube API client.

        Return YouTube API object.
        """
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)
        return youtube






    def getVideoInfo(self, videos_list):
        videos = []
        for yt_vid in videos_list:
            video = self.createVideoObject(yt_vid)
            videos.append(video)
        return videos

    def runYouTubeQuery(self, query, pages=0, maxResults=50, order="relevance"):
        """Retrieves video feed based on query build on YouTube service.

        :param query:
            YouTube Query class item, object.

        Return list with video feed.
        """
        ytService = self._setUpYTApi()  
        try:
            print "Searching YouTube for: %s" % query
            feed = ytService.search().list(
                q = query,
                part = "id,snippet",
                maxResults = maxResults,
                order = order
            ).execute()
            return feed.get("items", [])
        except Exception as e:
            print "[YouTube][runYouTubeQuery] Error: %s" % e
            return None
        

    def getVideo(self, id_video = ["2NxVMcf6TFc"]):
        """Get YouTube video by id.
        
        :param id:
           11-chars matching specific YouTube video ID, string.
 
        Return :class:`gdata.youtube` object with video entry.
        """
        ytService = self._setUpYTApi()

        videos_list_response = ytService.videos().list(
            id=",".join(id_video),
            part='id,contentDetails,statistics,snippet,recordingDetails,player'
        ).execute()
        return videos_list_response['items']
        #res = []
        #for i in videos_list_response['items']:
        #   pprint(i)
        #   temp_res = dict(v_id = i['id'])#, v_title = videos[i['id']])
        #   temp_res.update(i['statistics'])
        #   res.append(temp_res)
        #return res

    def convertDuration(self, duration):
        """PT1H21M46S"""
        import re
        try:
            p = re.compile('(\d+\w)')
            dur = p.findall(duration)
            dur_in_sec = 0
            hr = 0
            mt = 0
            sec = 0
            for t in dur:
                if "H" in t:
                    hr = int(t.replace('H','')) * 60 * 60
                elif "M" in t:
                    mt = int(t.replace('M','')) * 60
                elif "S" in t:
                    sec = int(t.replace('S',''))
            dur_in_sec = hr + mt + sec
            
        except Exception as e:
            print "[convertDuration] Error: %s" % e
            dur_in_sec = 0
        return dur_in_sec


        
    def createVideoObject(self, root = "",start_time = "", img_var = "", entry = {},program_title = "",extra_words = [], program_description="", event = {}, item = {}, q = "Query which got the video", date = "", cp = "", category = ""):
        """Instantiate :class:`Video` objects.

        :param entry:
            YouTube video information, object.

        :param event:
            It is :class:`Event` instance, object.

        :param q:
            Query which got the video, string.

        Return :class:`Video` object.
        """
        import Video
        yt_prefix = 'http://www.youtube.com/embed/'
        video = Video.Video()
        try:
            video.title = entry['snippet']['title']
        except:
            pass
        try:
            video.category_yt = entry.media.category[0].label
        except:
            pass
        try:
            video.category = category
        except:
            pass
        try:
            video.query = q
        except:
            pass
        try:
            video.channel_id = entry['snippet']['channelId']
        except:
            pass
        try:
            video.url = yt_prefix+entry['id']
        except Exception as e:
            print e

            pass

        try:
            video.root = root
        except:
            pass

        try:        
            dur = entry['contentDetails']['duration']
        except Exception as e:
            print "No duration 1, playlist: %s" %e
            
            pass
        try:
            self.convertDuration(dur)
            video.duration = self.convertDuration(dur)
            #print "duration: %s"%video.duration
        except Exception as e:
            print "No duration: %s" %e
            video.duration = 0

        try:
            video.description = entry['snippet']['description']
        except:
            pass

        try:
            video.tags = entry['snippet']['tags']
        except:
            video.tags = []
            pass

        try:
            video.image = entry['snippet']['thumbnails']['high']['url']
        except:
            pass
        try:
            video.channel_title = entry['snippet']['channelTitle']
        except:
            pass

        try:
            video.upload_date = entry['snippet']['publishedAt']
        except:
            pass
        try:
            video.uploader = entry.author[0].name.text
        except:
            pass

        try:
            video.item = item
        except:
            pass

        try:
            video.img_var = img_var
        except:
            pass
        try:
            video.viewcount = entry['statistics']['viewCount']
        except:
            pass
        
        try:
            if len(date) == 0:
                video.date = items_name['data']
            else:
                video.date = date
        except:
            video.date = date
        return video