# -*- coding: utf-8 -*-
import scrapy

class NewsNowItem(scrapy.Item):
    country = scrapy.Field()
    date = scrapy.Field()
    time = scrapy.Field()
    description = scrapy.Field()
    title = scrapy.Field()
    url = scrapy.Field()
    videos = scrapy.Field()
    source = scrapy.Field()
    pass
