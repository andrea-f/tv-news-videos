# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


import json
from spiders.exceptions import JSONReadError
from scrapy.exceptions import DropItem
import datetime
import hashlib
import pprint
import YouTube
import urllib
import unirest
import time

class CheckDataValidityPipeline(object):

	def open_spider(self, spider):
		pass

	def close_spider(self, spider):
		pass

	def process_item(self, entry, spider):
		""" 
		Checks item before saving it.
		
		:param spider:
			Spider crawler instance, class.
		
		:param entry:
			Data to save, dict.
		
		
		"""
		key_types = {
			'int': int,
			'list': list,
			'str': unicode,
			'date': unicode
		}
		c = 0
		if len(entry.keys()) < spider.config['scrape']['max_missing_mappings']:
			raise DropItem(" Object in pipeline missing required key: %s, dropping item" % entry)
		for key in spider.config['scrape']['object_mapping'].keys():			
			if not isinstance(entry[key], key_types[spider.config['scrape']['object_mapping'][key]['type']]):
				raise DropItem("Dropping item: %s because key %s not right type: %s, expected: %s" % (entry,key, type(entry[key]), spider.config['scrape']['object_mapping'][key]['type']) )
		return entry
			
class VideoFinderPipeline(object):
	f = open("/Users/afassina/Workspace/private/NewsNow/newsnow-scraper-scrapy/newsnow_scraper/newsnow_scraper/mashape_api_key","r")
	MASHAPE_KEY = f.read()
	f.close()

	def __init__(self):
		self.youtube = YouTube.SearchVideos()
		self.minimum_characters = 3

	def open_spider(self, spider):
		pass

	def close_spider(self, spider):
		pass

	def extract_entities(self, txt, spider):
		# These code snippets use an open-sFource library.
		#print txt
		base_url = u'https://topics-extraction.p.mashape.com/topics-2.0.php?dm=s&lang=en&of=json&rt=n&sdg=l&st=n&tt=a&txt={}&txtf=plain&uw=n'
		txt = txt.replace('/2018','').replace('/2019'," ")
		try:
			txt = txt.encode("utf8")
		except:
			pass
		try:
			url = base_url.format(txt)
		except:
			url = base_url.format(txt.decode('ascii', 'ignore'))
		headers = {
		    "X-Mashape-Key": self.MASHAPE_KEY,
		    "accept": "application/json;"
		}
		try:
			response = unirest.get(
			  url, 
			  headers=headers
			)
		except Exception as e:
			print "Error in requesting entities: %s" % e
			

		resp = response.body
		try:
			entities = [ent['form'] for ent in resp['entity_list']]
		except:
			entities = []
			pass
		try:
			concepts = [ent['form'] for ent in resp['concept_list']]
		except:
			concepts = []
			pass
		try:
			relevant = [ent['form'] for ent in resp['relation_list'][0]['complement_list']]
		except:
			relevant = []
			pass
		return {
			"entities": entities,
			"concepts": concepts,
			"relevant": relevant
		}


	def has_required(self, entry, feed, query, spider):
		"""Check that it matches timeframe"""
		try: 
			days_range = spider.config['scrape']['days_range']
		except: 
			days_range = 10
		try:
			minimum_matching = spider.config['scrape']['minimum_matching']
		except:
			minimum_matching = 2
		
		outlist = []
		words_list = [w.lower() for w in entry['description'].split() if len(w) >= self.minimum_characters]
		title = entry['title'].lower()
		words_list.append(title)
		#print "WORDLIST: %s"%words_list
		for video in feed:
			uploaded = video['snippet']['publishedAt']
			uploaded_in_ms = datetime.datetime.strptime(uploaded, "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%s')
			uploaded_in_ms = int(uploaded_in_ms) * 1000
			matching = self.count_matching(words_list, video)
			try:
				video_id = video['id']['videoId']
				# Find difference in days between when the news was found to when the video was uploaded
				if ((abs(int(entry['date']) - uploaded_in_ms) / 1000 / 60 / 60 / 24) < days_range and
					not video_id in outlist and
						matching >= minimum_matching):
							outlist.append(video_id)
			except Exception as e:
				print e
				pass
		return outlist

	def count_matching(self, words_list, video):
		""" Returns number of matching elements from item description in video"""
		matching = 0
		desc = video['snippet']['description'].lower()
		tit = video['snippet']['title'].lower()
		for word in words_list:
			if word in desc or word in tit:
				matching += 1
		return matching

	def process_item(self, entry, spider, over_write=False):
		
		# Improve video query creation
		# than just using description
		query = entry['description']
		if len(query) > 0:
			feed = self.youtube.runYouTubeQuery(query=query, order = "date")
			ids = self.has_required(entry, feed, query, spider)
			if len(ids) == 0:
				retrieved = self.extract_entities(entry['description'], spider)
				if len(retrieved['entities']) > 1:
					query = ' '.join(retrieved['entities'])
					feed = self.youtube.runYouTubeQuery(query=query, order = "date")
					ids = self.has_required(entry, feed, query, spider)
				if len(ids) == 0:
					query = "%s %s %s" % (query, ' '.join(retrieved['concepts']), ' '.join(retrieved['relevant']))
					query = query.strip()
					feed = self.youtube.runYouTubeQuery(query=query, order = "date")
					ids = self.has_required(entry, feed, query, spider)
			#if len(ids) > 0:
				#print ":::::::::::::::::::::::::::::::: IDS: %s" % ids
			# videos with more info
			feed_with_info = self.youtube.getVideo(id_video=ids)
			words_list = [w.lower() for w in entry['description'].split() if len(w) >= self.minimum_characters]
			title = entry['title'].lower()
			words_list.append(title)
			ordered_videos = []
			for video in feed_with_info:
				matching = self.count_matching(words_list, video)
				video['matching'] = matching
				#video['hash'] = hashlib.md5("{}{}" % (entry['description'], video['id'])).hexdigest()
				ordered_videos.append((video, matching))

			best_videos = sorted(ordered_videos, key=lambda x: x[1])
			most_viewed = sorted([item[0] for item in best_videos], key=lambda x: int(x["statistics"]['viewCount']))
			if len(most_viewed) > 0:
				try:
					if len(most_viewed) >= spider.config['scrape']["save_max_videos"]:
						stop = int(spider.config['scrape']["save_max_videos"]) - 1
						most_viewed = most_viewed[:stop]
				except:
					pass
				entry['videos'] = most_viewed#[item[0] for item in best_videos]
				if len(entry['videos']) > 0:
					spider.saved_videos += len(entry['videos'])
					print "::::::::: Saved videos: %s" % spider.saved_videos
		return entry

class JsonWriterPipeline(object):

	def open_spider(self, spider):
		#now = time.strftime("%c").replace(' ', '_')
		self.file_name = './data/full_output_scrapy_with_videos.json'

	def close_spider(self, spider):
		pass

	def process_item(self, entry, spider, over_write = False):
		""" 
		Writes json file out.
		
		:param file_name:
			What is the file name to read/create, string.
		
		:param entry:
			Data to save, dict.
		
		:param over_write:
			Whether to add to the file or create it fresh, defaults to False, boolean.
		
		"""
		file_name = self.file_name
		entry = dict(entry)
		if not over_write:
			try:
				f = open(file_name, 'r')
				data = json.load(f)
				f.close()
				try:
					data.append(entry)
				except:
					data = [entry]
			except Exception as e:
				data = [entry]
				#raise JSONReadError("Error in pipeline to append to current json: %s" % e)
				pass
		else:
			data = [entry]
		f = open(file_name, 'w+')
		f.write(json.dumps(data, indent=4, sort_keys=True, default=unicode))
		f.close()
		print("Wrote %s lines in %s" %(len(data), file_name) )
		return entry
