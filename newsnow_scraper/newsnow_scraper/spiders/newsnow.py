# -*- coding: utf-8 -*-
import os
import sys
import json
import logging
from datetime import datetime
from types import NoneType
import scrapy
from newsnow_scraper.spiders.exceptions import (
    HTMLParseError,
    NoMatchedKeys,
    CookiesInformationError,
    RequiredConfigKey,
    JSONReadError,
    PostValidationError,
    WrongTypeError,
    MaxNestedObjectsInInput,
    NotAFullURL,
    FARAItemCreationError
)
import pprint
import time
from scrapy.selector import Selector
from scrapy.http import FormRequest, Request
from newsnow_scraper.items import NewsNowItem
from scrapy.utils.log import configure_logging
configure_logging(install_root_handler=False)
logging.basicConfig(
    filename='log.txt',
    filemode='a',
    format='%(levelname)s: %(message)s',
    level=logging.DEBUG
)


class FaraActivePrincipalsSpider(scrapy.Spider):

    name = "newsnow"
    custom_settings = {'REDIRECT_MAX_TIMES': 10}


    def __init__(self, file_name="input.json"):
        """
        Initialise class variables and create workers

        :param file_name:
            Where to read the input config from the file system, string.
        """
        self.input_config = file_name
        self.saved_videos = 0
        # Holds config information
        self.config = {}
        # It would be probably a good idea to move this out into its own class
        # self.object_mapping will be populated once config gets read in
        self.object_mapping = {}
        # Initialise array here for async requests
        self.sub_pages = []
        self.counter = 0
        #self.hashes = []


    def clear_workspace(self):
        # Move last latest and 
        output_location = "./data/full_output_scrapy_with_videos"
        old_name = output_location+".json"
        print os.getcwd(),old_name, os.path.isfile(old_name)
        if os.path.isfile(old_name):
            now = time.strftime("%c").replace(' ', '_')
            new_name = output_location+"_"+now+".json"
            print "::::: RENAMING: %s to %s" % (old_name, new_name)
            os.rename(old_name, new_name)
            return new_name
        return False


    def read_config_file(self, file_name=""):
        """
        Reads json file with dbpedia query information

        :param filename:
            Location of JSON input file or a file like object like JSON in quotes
            with specified parameters to run scraper, string.
        Returns read JSON data.
        """
        if len(file_name) == 0:
            file_name = self.input_config
        if not os.path.isfile(file_name):
            file_name = "%s/spiders/%s" % (os.getcwd(), file_name)
        try:
            with open(file_name, "r") as json_file:
                data = json.load(json_file)
                return data
        except:
            raise JSONReadError("[getConfigFile] Error in reading file: %s, quitting." % file_name)
            sys.exit(2)

    def __save_cookie_info(self, cookie):
        """
        Splits retrieved cookie values and assigns them to input config.
        Sets cookies in input config.

        :param cookie:
            Holds cookies retrieved from server response headers, string.
        """
        try:
            if isinstance(cookie, str):
                key = cookie.split('=')[0]
                val = cookie.split('=')[1]
                if len(key) > 0 and len(val) > 0:
                    self.config['api_headers']['cookies'][key] = val
        except Exception as e:
            raise CookiesInformationError("Error in splitting and assigning cookie %s" % e)

    def run_scrape_in_environment(self, response):
        """
        Set cookies so data can be requested.

        :param response:
            Contains HTTP response to request, class.
        """
        cookies = {}
        hxs = Selector(response=response)
        # Add any parameters in the body of the request to the input config

        def __get_root_body_payload(key):
            """
            Retrieves relevant keys set in the body response to the get function.

            :param key:
                Identifier to what information must be scraped from root page, string.
            """
            selector = self.config['root']['object_mapping'][key]['rule']
            try:
                values = hxs.xpath(selector).extract()
            except:
                raise HTMLParseError("Error in parsing HTML data with selector: %s" % selector)
            if len(values) > 0:
                # Assuming here there will be at least one variable
                self.config["api_post_data"][key] = values[0]
        try:
            if self.config['root']['object_mapping']: object_mapping = True
        except:
            object_mapping = False
        if object_mapping:
            map(__get_root_body_payload, self.config['root']['object_mapping'].keys())

        # Get any cookies from the response to be reused
        set_from_headers = response.headers.getlist('Set-Cookie')
        if len(set_from_headers) > 0:
            map(self.__save_cookie_info, set_from_headers)

        url = self.config['root']['url']
        if response.status and response.status != 200:
            # The clause here that if its a redirect url,
            # then call this function as callback again to yield
            # header with Location for redirection with cookies.
            try:
                if not response.headers['Location'] in url:
                    url = "%s%s" % (url, response.headers['Location'])
            except:
                pass
            if not "http" in url:
                raise NotAFullURL("%s is relative!" % url)
            yield Request(
                url,
                callback=self.run_scrape_in_environment,
                cookies=cookies,
                meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
            )
        else:
            #headers = self._validate_headers(self.config['api_headers'])
            yield FormRequest(
                url,
                callback=self.parse,
                #formdata=post_data,
                #headers=headers,
                cookies=cookies,
                method="GET",
                meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
            )

    def start_requests(self):
        """
        First function to be called by scrapy!
        Built in scrapy function necessary in subclass of scrapy.Spider()
        """
        # Verify input configuration file
        if len(self.config) == 0:
            self.config = self.read_config_file()
        self.clear_workspace()
        yield FormRequest(
            self.config['root']['url'],
            #callback=self.run_scrape_in_environment,
            callback=self.parse,
            formdata={},
            #headers=self._validate_headers(self.config['root']['headers']),
            method="GET",
            meta={'dont_redirect': True, 'handle_httpstatus_list': [302]}
        )

    def parse(self, response,  row={}):
        """
        Parses repsonse from Scrapy scraping.

        :param response:
            Contains HTTP response to request, class.
        :param entries:
            Will hold scraped data, dict.
        :param row:
            Initializes empty var, will hold single extracted line, dict.
        Returns function call to process subpages
        """
        entries = {}
        fake_group = 0
        hxs = Selector(response=response)
        try:
            selector = self.config['scrape']['route_to_data']
            try:
                data_container = hxs.xpath(selector)
            except:
                data_container = hxs.css(selector)
        except Exception as e:
            print e
            data_container = hxs.xpath('//body')
        #print response.body
        for item in data_container:
            sub_items = {}
            for key, val in self.config['scrape']['object_mapping'].iteritems():
                try:
                    try:
                        extracted = item.xpath(val['rule']).extract()
                    except:
                        extracted = item.css(val['rule']).extract()
                    print extracted, val['rule'], key
                except Exception as e:
                    raise HTMLParseError("Error in parsing HTML data with selector: %s - %s" % (val['rule'], e))
                #from lxml import html
                
                if self.config['scrape']['group_by'] in key:
                    #print ''.join([html.tostring(child) for child in item._root.iterdescendants()])
                    if len(extracted) == 1:
                        entries[extracted[0]] = sub_items
                        sub_items = {}
                    #else:
                     #   entries["%s %s" % (self.config['scrape']['category'], fake_group)] = sub_items
                      #  if len(sub_items) >= (int(self.config['scrape']['max_missing_mappings']) - 1):  
                       #     fake_group += 1                 
                        #    sub_items = {}
                else:
                    sub_items[key] = extracted
                    
                    if len(sub_items) >= (int(self.config['scrape']['max_missing_mappings'])):  
                        entries["%s %s" % (self.config['scrape']['category'], fake_group)] = sub_items
                        fake_group += 1                      
                        sub_items = {}
                    
        new_list = []
        print len(entries)
        for key,sub_items in entries.iteritems():
            matrix = []
            for item in sub_items.keys():
                matrix.append(sub_items[item])
            #print matrix

            merged = zip(*matrix)
            print merged
            for each in merged:
                # Item is actually created here: 
                v = {self.config['scrape']['group_by']:key}
                c = 0
                while c < len(each):
                    try:
                        v[sub_items.keys()[c]] = each[c]
                    except:
                        v[sub_items.keys()[c]] = None
                    c += 1
                if not "date" in sub_items.keys():
                    d = u'date'
                    v[d] = unicode(int(round(time.time() * 1000)))
                new_list.append(v)
        #pprint.pprint(new_list)
        for item in new_list:
            entry = NewsNowItem(**item)
            # Send to next step in pipeline
            yield entry




