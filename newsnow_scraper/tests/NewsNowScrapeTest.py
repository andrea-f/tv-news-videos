import unittest
import pprint
import sys, os
import mock
import scrapy
from copy import deepcopy
import inspect
from types import GeneratorType
from scrapy.http import Response, FormRequest, Request, HtmlResponse
try:
    sys.path.append('../')
    from NewsNow.spiders import NewsNow_active_principals, exceptions
    from NewsNow.pipelines import JsonWriterPipeline
except:
    pprint.pprint(sys.path)
from scrapy.selector import Selector



class ScrapeTestConfig(unittest.TestCase):
    """ Test code with currently available NewsNow website data """

    def setUp(self):
        self.spider = NewsNow_active_principals.NewsNowActivePrincipalsSpider()
        self.spider.config = self.spider.read_config_file(file_name="input_test.json")
        
    #@unittest.skip("test_json_read")
    def test_json_read(self):
        """ Should raise a JSONReadError error if file is not found or malformed"""
        self.assertRaises(exceptions.JSONReadError, self.spider.read_config_file, "/xyz.json")
    
    #@unittest.skip("test_config_read_and_check_required")
    def test_config_read_and_check_required(self):
        """ Should PASS if input_test.json has all required keys"""
        config = self.spider.read_config_file(file_name="input_test.json")
        has_all_config_keys = self.spider.check_required(input_config=config, required = self.spider.get_required_keys())
        self.assertEqual(has_all_config_keys, True)

    #@unittest.skip("test_missing_keys")
    def test_missing_keys(self):
        """ Should raise an RequiredConfigKey error """
        self.assertRaises(exceptions.RequiredConfigKey, self.spider.check_required, {"whatever":1234}, self.spider.get_required_keys())

   
    @mock.patch('scrapy.http.response')
    def _mock_response(self,response, text = ""):
        # Would be better to have encoding from page!
        response.text = unicode(text)
        response.url = "http://just.testing/"
        response.meta = {}
        return response

    #@unittest.skip("test_yielding_a_request_form_run_scrape_in_environment")
    def test_yielding_a_request_form_run_scrape_in_environment(self):
        """ Test that the run_scrape_in_environment method yields a request object """
        #html_page = open('page_detail.html', 'r').read()
        res = isinstance(list(self.spider.run_scrape_in_environment(self._mock_response()))[0], scrapy.http.Request)   
        self.assertEqual(True, res)
    
    @mock.patch('NewsNow.spiders.NewsNow_active_principals.NewsNowActivePrincipalsSpider.run_sub_pages')
    #@unittest.skip("test_retrieving_an_object_from_HTML")
    def test_retrieving_an_object_from_HTML(self, mock):
        """ Should find an object from the mock HTML response """
        text = open('page_detail.html', 'r').read()
        response = self._mock_response(text = text)
        # input:
        response.meta["item"] = {
            "address": "Shibakoen Ridge Building", 
            "country": "Japan", 
            "date": "2015-06-02T00:00:00", 
            "exhibit_url": None, 
            "foreign_principal": "Government Of Japan Through Kreab Gavin Anderson K.K.", 
            "reg_num": "6293", 
            "registrant": "Finsbury, Llc", 
            "state": None, 
            "url": "https://www.newsnow.co.uk/pls/apex/f?p=171:200:35090111232218::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_COUNTRY:6293,Exhibit%20AB,JAPAN"
        }
        expected_output = {
            "address": "Shibakoen Ridge Building", 
            "country": "Japan", 
            "date": "2015-06-02T00:00:00", 
            "exhibit_url": ['http://www.newsnow.co.uk/docs/6332-Exhibit-AB-20151223-1.pdf'], 
            "foreign_principal": "Government Of Japan Through Kreab Gavin Anderson K.K.", 
            "reg_num": "6293", 
            "registrant": "Finsbury, Llc", 
            "state": None, 
            "url": "https://www.newsnow.co.uk/pls/apex/f?p=171:200:35090111232218::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_COUNTRY:6293,Exhibit%20AB,JAPAN"
        }
        self.spider.look_for_missing_data(response)
        self.spider.run_sub_pages.assert_called_with(counter = 1, item = expected_output)
        pass


    #@unittest.skip("test_find_exhibit_url_in_sub_page")
    @mock.patch('NewsNow.spiders.NewsNow_active_principals.NewsNowActivePrincipalsSpider.run_sub_pages')
    def test_find_exhibit_url_in_sub_page(self, mock):
        """ Should complete by calling run_sub_pages with a counter of 1 """
        text = open('page_detail.html', 'r').read()
        response = self._mock_response(text = text)
        response.meta["item"] = {}
        self.spider.look_for_missing_data(response)
        s = self.spider.run_sub_pages.call_args_list
        # What run_sub_pages will be called with after processing.
        exhibit_url = s[0][1]['item']['exhibit_url']
        assert len(exhibit_url) > 0

    #@unittest.skip("test_must_have_matching")
    def test_must_have_matching(self):
        """ 
        Should raise NoMatchedKeys error, 
        when keys of already populated item are checked in latest retrieved data and none is found.
        
        """
        text = open('page_detail.html', 'r').read()
        response = self._mock_response(text = text)
        response.meta["item"] = {
            "address": "Shibakoen Ridge Building", 
            "country": "Japan", 
            "date": "2015-06-02T00:00:00", 
            "exhibit_url": None, 
            "foreign_principal": "Government Of Japan Through Kreab Gavin Anderson K.K.", 
            "reg_num": "6332", 
            "registrant": "Finsbury, Llc", 
            "state": None, 
            "url": "https://www.newsnow.co.uk/pls/apex/f?p=171:200:35090111232218::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_COUNTRY:6293,Exhibit%20AB,JAPAN"
        }
        self.spider.config['scrape']['must_have_matching'] = True
        self.assertRaises(exceptions.NoMatchedKeys, self.spider.look_for_missing_data, response)

    #@unittest.skip("test_start_crawler")
    def test_start_crawler(self):
        """ Should return a GeneratorType class instance if the start_requests method is called """
        res = isinstance(self.spider.start_requests(), GeneratorType)
        self.assertEqual(res, True)
        pass

    #@unittest.skip("test_json_object_is_saved")
    def test_json_object_is_saved(self):
        """ Should save a json object, adding to a file if the file exists via scrapy pipeline, and return item"""
        obj = JsonWriterPipeline()
        entry = {"test_key":"test_value"}
        obj.file_name = "%s/json_write_test.json" % (os.getcwd())
        class mock_spider():
            """ Mocks spider class """
        item = obj.process_item(entry, mock_spider())
        self.assertEqual(os.path.isfile(obj.file_name),True)
        self.assertEqual(isinstance(item, dict), True)
        pass

    #@unittest.skip("test_parsing_a_response_parse")
    @mock.patch('NewsNow.spiders.NewsNow_active_principals.NewsNowActivePrincipalsSpider.scrape_further_data')
    def test_create_an_object_from_raw_HTML(self, mock):
        """ Tests that sub_pages (containing scraped items) is larger than 0 with input raw HTML """
        text = open('sample_html_entry.html', 'r').read()
        response = self._mock_response(text = text)
        self.spider.parse(response)
        expected_reg_num = "6167"
        expected_date = "05/03/2013"
        expected_url = "https://www.newsnow.co.uk/pls/apex/f?p=171:200:16328121970662::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_COUNTRY:6167,Exhibit%20AB,EL%20SALVADOR"        
        s = self.spider.scrape_further_data.call_args_list
        url = s[0][0][0][0]["url"]
        date = s[0][0][0][0]["date"]
        reg_num = s[0][0][0][0]["reg_num"]
        assert expected_reg_num in reg_num
        assert expected_date in date
        assert expected_url in url
        pass

    #@unittest.skip("test_create_an_item_from_dict")
    def test_create_an_item_from_dict(self):
        """ Should create a NewsNowItem from dict given response """
        item = {
            "address": "Shibakoen Ridge Building", 
            "country": "Japan", 
            "date": "2015-06-02T00:00:00", 
            "exhibit_url": None, 
            "foreign_principal": "Government Of Japan Through Kreab Gavin Anderson K.K.", 
            "reg_num": "6332", 
            "registrant": "Finsbury, Llc", 
            "state": None, 
            "exhibit_url": [
                "http://www.newsnow.co.uk/docs/1032-Exhibit-AB-19680401-CYXK1I02.pdf", 
                "http://www.newsnow.co.uk/docs/1032-Exhibit-AB-19570601-CYYFQW03.pdf"
            ], 
            "url": "https://www.newsnow.co.uk/pls/apex/f?p=171:200:35090111232218::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_COUNTRY:6293,Exhibit%20AB,JAPAN"
        }
        res = isinstance(self.spider.run_sub_pages(item = item, stop = False), GeneratorType)
        assert res is True
        
    @unittest.skip("test_non_200_response_status")
    def test_non_200_response_status(self):
        """ Should call function twice if response.status is not 200. """
        text = open('sample_html_entry.html', 'r').read()
        response = self._mock_response(text = text)
        response.status = 404
        self.spider.run_scrape_in_environment(response)
        assert self.spider.run_scrape_in_environment.called
        pass

    def tearDown(self):
        self.spider = None
