# README #

TV News Videos is a platform to aggregate news from around the world and present in a video, news channel format. All done automatically. News information are updated once per hour.
Demo is at http://52.208.102.230/output_player.html#

### What is this repository for? ###

* Create a video news channel, cast automatically latest news around the world using NER, YouTube API, News sites.
* Technologies used: Python, Javascript, YouTube API, Scrapy, HTML, CSS

### How do I get set up? ###

## Requirements ##
* `sudo yum install -y libffi libffi-devel`

* `sudo yum install libxml2-devel libxslt-devel`

* `sudo yum install openssl-devel`

* `sudo yum install httpd mod_ssl`

- set `export GOOGLE_APPLICATION_CREDENTIALS=<LOCATION_OF>/credentials.json` or modifiy the location in YouTube.py `os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "<LOCATION_OF>/credentials.json"`

- Create youtube_api_key file in same folder as YouTube.py, and put the YouTube Data API key

- Create mashape_api_key file in same folder as pipelines.py, and put the Mashape API key

- Copy output_player.html to /var/www/html

- Create data folder: `sudo mkdir /var/www/html/data`

- Create symbolic link from output folder to /var/www/html/data/full_output_scrapy_with_videos.json

- Make sure oauth2client==1.5.0 is installed

- To have the service run by itself set up a cron job to run every hour: `0 * * * * cd /home/ec2-user/tv-news-videos && /home/ec2-user/tv-news-videos/newsnow/bin/python scrapy crawl newsnow > /tmp/cronlog.txt 2>&1`

### How do I get set up? ###
* Clone repo
* Install virtualenv requirements
* navigate to newsnow_scraper and run: `scrapy crawl newsnow`
* Scraping configuration in input.json
* Output in JSON, with videos


### Who do I talk to? ###

* http://www.walkabout.xyz